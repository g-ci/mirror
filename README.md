# Mirror

Mirroring strategy

# How to use it?

## push.yml

TL;DR;

Put it into your .gitlab-ci.yml

```yaml
include: 'https://gitlab.com/g-ci/mirror/-/raw/master/push.yml'
variables:
  MIRROR_TARGET_URL: '<your.gitlab.hosted/repo2.git>'
```

![explain](https://gitlab.com/g-ci/mirror/-/snippets/2001496/raw)

## pull.yml

TL;DR;

Put it into your .gitlab-ci.yml

```yaml
include: https://gitlab.com/g-ci/mirror/-/raw/master/pull.yml
variables:
  MIRROR_SOURCE_URL: '<your.gitlab.hosted/repo1.git>'
  MIRROR_TARGET_URL: '<your.gitlab.hosted/repo2.git>'
```

![explain](https://gitlab.com/g-ci/mirror/-/snippets/2001501/raw)
